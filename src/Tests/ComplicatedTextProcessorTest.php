<?php

namespace TextMicroservice\Tests;

use TextMicroservice\Services\ComplicatedTextProcessor\ComplicatedTextProcessor;

class ComplicatedTextProcessorTest extends \PHPUnit\Framework\TestCase
{

    public function testProcess()
    {
        $text = "Привет, мне на <a href=\"test@test.ru\">test@test.ru</a> пришло приглашение встретиться, попить кофе с <strong>10%</strong> содержанием молока за <i>$5</i>, пойдем вместе!";
        $methods = [
            "stripTags", "removeSpaces", "replaceSpacesToEol", "htmlspecialchars", "removeSymbols", "toNumber"
        ];
        $assertion = 10;

        $processor = new ComplicatedTextProcessor();
        $this->assertEquals($processor->process($text, $methods), $assertion);
    }

    public function testProcessStripTags()
    {
        $text = "Привет, мне на <a href=\"test@test.ru\">test@test.ru</a> пришло приглашение встретиться, попить кофе с <strong>10%</strong> содержанием молока за <i>$5</i>, пойдем вместе!";
        $methods = [
            "stripTags"
        ];
        $assertion = 'Привет, мне на test@test.ru пришло приглашение встретиться, попить кофе с 10% содержанием молока за $5, пойдем вместе!';

        $processor = new ComplicatedTextProcessor();
        $this->assertEquals($processor->process($text, $methods), $assertion);
    }

    public function testProcessRemoveSpaces()
    {
        $text = "Привет, мне на <a href=\"test@test.ru\">test@test.ru</a> пришло приглашение встретиться, попить кофе с <strong>10%</strong> содержанием молока за <i>$5</i>, пойдем вместе!";
        $methods = [
            "removeSpaces"
        ];
        $assertion = 'Привет,мнена<ahref="test@test.ru">test@test.ru</a>пришлоприглашениевстретиться,попитькофес<strong>10%</strong>содержаниеммолоказа<i>$5</i>,пойдемвместе!';

        $processor = new ComplicatedTextProcessor();
        $this->assertEquals($processor->process($text, $methods), $assertion);
    }
}
