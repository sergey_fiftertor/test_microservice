<?php

namespace TextMicroservice\Services\TextProcessor;

class HtmlSpecialChars implements TextProcessor
{
    public function process(string $text): string
    {
        return htmlspecialchars($text);
    }
}