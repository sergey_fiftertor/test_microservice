<?php


namespace TextMicroservice\Services\TextProcessor;

class TextProcessorFactory
{
    public function create(string $method): TextProcessor
    {
        switch ($method) {
            case 'htmlspecialchars':
                return new HtmlSpecialChars();
            case 'removeSpaces':
                return new RemoveSpaces();
            case 'removeSymbols':
                return new RemoveSymbols();
            case 'replaceSpacesToEol':
                return new ReplaceSpacesToEol();
            case 'stripTags':
                return new StripTags();
            case 'toNumber':
                return new ToNumber();
            default:
                throw new TextProcessorException('TextProcessor doesn\'t exist for: ' . $method);
        }
    }
}