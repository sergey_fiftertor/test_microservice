<?php

namespace TextMicroservice\Services\TextProcessor;

class RemoveSymbols implements TextProcessor
{
    public function process(string $text): string
    {
        return str_replace(
            ['[', '.', ',', '/', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', ']'],
            '',
            $text
        );
    }
}