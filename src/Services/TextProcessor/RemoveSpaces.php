<?php

namespace TextMicroservice\Services\TextProcessor;

class RemoveSpaces implements TextProcessor
{
    public function process(string $text): string
    {
        return str_replace(' ', '', $text);
    }
}