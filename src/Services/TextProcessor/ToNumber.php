<?php

namespace TextMicroservice\Services\TextProcessor;

class ToNumber implements TextProcessor
{
    public function process(string $text): int
    {
        preg_match('/-?\d+/', $text, $match);
        if (!isset($match[0])) {
            throw new TextProcessorException('No number found');
        }
        return (int)$match[0];
    }
}