<?php

namespace TextMicroservice\Services\TextProcessor;

class ReplaceSpacesToEol implements TextProcessor
{
    public function process(string $text): string
    {
        return str_replace(' ', PHP_EOL, $text);
    }
}