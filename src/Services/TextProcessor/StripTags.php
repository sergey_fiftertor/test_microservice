<?php

namespace TextMicroservice\Services\TextProcessor;

class StripTags implements TextProcessor
{
    public function process(string $text): string
    {
        return strip_tags($text);
    }
}