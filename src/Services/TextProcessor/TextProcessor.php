<?php

namespace TextMicroservice\Services\TextProcessor;

interface TextProcessor
{
    /**
     * @param string $text
     * @return string|int
     */
    public function process(string $text);
}