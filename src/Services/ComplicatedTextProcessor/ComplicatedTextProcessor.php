<?php

namespace TextMicroservice\Services\ComplicatedTextProcessor;

use TextMicroservice\Services\TextProcessor\TextProcessorException;
use TextMicroservice\Services\TextProcessor\TextProcessorFactory;

class ComplicatedTextProcessor
{

    /**
     * @param string $message
     * @param array $methods
     * @return int|string
     * @throws TextProcessorException
     */
    public function process(string $message, array $methods)
    {
        $factory = new TextProcessorFactory();
        foreach ($methods as $method) {
            $message = ($factory->create($method))->process($message);
        }
        return $message;
    }
}