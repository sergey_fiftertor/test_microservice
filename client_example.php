<?php

$client = new GearmanClient();
$client->addServer('127.0.0.1', '4730');

$text = "Привет, мне на <a href=\"test@test.ru\">test@test.ru</a> пришло приглашение встретиться, попить кофе с <strong>10%</strong> содержанием молока за <i>$5</i>, пойдем вместе!";
$methods = [
    "stripTags", "removeSpaces", "replaceSpacesToEol", "htmlspecialchars", "removeSymbols", "toNumber"
];
$data = json_encode([
    'job' => [
        'text' => $text,
        'methods' => $methods,
    ]
]);
$result = $client->doNormal('process_text', $data);

print $result;