<?php

require __DIR__ . '/vendor/autoload.php';

use TextMicroservice\Services\ComplicatedTextProcessor\ComplicatedTextProcessor;

$worker = new GearmanWorker();
$worker->addServer();
$worker->addFunction('process_text', 'do_job');

while (true) {
    $worker->work();
    if ($worker->returnCode() != GEARMAN_SUCCESS) {
        break;
    }
}

function do_job($job)
{
    try {
        $workload = $job->workload();
        $data = json_decode($workload, true);
        if (!isset($data['job']['text']) || !isset($data['job']['methods'])) {
            throw new Exception('Invalid input');
        }

        $processor = new ComplicatedTextProcessor();
        $result = $processor->process($data['job']['text'], $data['job']['methods']);
        return json_encode(['text' => $result]);
    } catch (Exception $e) {
        return json_encode(['error' => $e->getMessage()]);
    }
}