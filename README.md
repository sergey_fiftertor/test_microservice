## Install vendors to run unit tests
        composer install
## Run unit tests
        vendor/phpunit/phpunit/phpunit --bootstrap vendor/autoload.php src/Tests
## Run microservice at 127.0.0.1:4730
        php index.php
## Example in client_example.php
        php client_example.php